import { createApp } from 'vue'
import { createPinia } from 'pinia'

// firebase setup
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// https://firebase.google.com/docs/web/setup#available-libraries

// Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDkyV4qX_Y_3PYNVJWSBxHjJUecjVkQOnU",
  authDomain: "gym-management-system-d5d3b.firebaseapp.com",
  projectId: "gym-management-system-d5d3b",
  storageBucket: "gym-management-system-d5d3b.appspot.com",
  messagingSenderId: "781193337336",
  appId: "1:781193337336:web:2b0bb0f3402fb6253d9e0d"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig)
// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(firebaseApp)
// Initialize Firestore
const db = getFirestore(firebaseApp)
// end firebase setup

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';


import App from './App.vue'
import router from './router'

import './assets/index.css'

const app = createApp(App)
window.db =  db;
window.auth =  auth;
app.use(VueSweetalert2)
window.Swal =  app.config.globalProperties.$swal;
app.use(createPinia())
app.use(router)

app.mount('#app')
