import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import SignUpView from '../views/SignUpView.vue'

import AdminLayout from '../views/AdminView.vue'
import AdminDashboard from '../components/AdminDashboard.vue'
import AdminMember from '../components/AdminMember.vue'
import AdminSingleMember from '../components/AdminSingleMember.vue'
import AdminTraining from '../components/AdminTraining.vue'
import AdminAddTraining from '../components/AdminAddTraining.vue'
import AdminAttendance from '../components/AdminAttendance.vue'
import AdminReports from '../components/AdminReports.vue'
import AdminReportAttendance from '../components/AdminReportAttendance.vue'
import AdminReportPayment from '../components/AdminReportPayment.vue'

import StudLayout from '../views/StudView.vue'
import StudDashboard from '../components/StudDashboard.vue'
import StudTraining from '../components/StudTraining.vue'
import StudProfile from '../components/StudProfile.vue'
import StudMembership from '../components/StudMembership.vue'
import StudSingleReport from '../components/StudSingleReport.vue'



const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },{
      path: '/sign-up',
      name: 'signUp',
      component: SignUpView
    },{
      path: '/admin',
      component: AdminLayout,
      children:[
        {
          path: '',
          redirect: '/admin/dashboard'
        },{
          path: 'dashboard',
          name: 'admin-dashboard',
          component: AdminDashboard
        },{
          path: 'member',
          name: 'admin-member',
          component: AdminMember
        },{
          path: 'member/:id',
          name: 'admin-single-member',
          component: AdminSingleMember
        },{
          path: 'training',
          name: 'admin-training',
          component: AdminTraining
        },{
          path: 'add-training/:id',
          name: 'admin-add-training',
          component: AdminAddTraining
        },{
          path: 'attendance',
          name: 'admin-attendance',
          component: AdminAttendance
        },{
          path: 'report',
          name: 'admin-report',
          component: AdminReports
        },{
          path: 'payment-report',
          name: 'admin-payment-report',
          component: AdminReportPayment
        },{
          path: 'attendance-report',
          name: 'admin-attendance-report',
          component: AdminReportAttendance
        }
      ]
    },{
      path: '/student',
      component: StudLayout,
      children:[
        {
          path: '',
          redirect: '/student/dashboard'
        },{
          path: 'dashboard',
          name: 'stud-dashboard',
          component: StudDashboard,
        },{
          path: 'training',
          name: 'stud-training',
          component: StudTraining
        },{
          path: 'profile',
          name: 'stud-profile',
          component: StudProfile
        },{
          path: 'membership',
          name: 'stud-membership',
          component: StudMembership
        },{
          path: 'report/:id',
          name: 'stud-single-report',
          component: StudSingleReport
        }
      ]
    }
  ]
})

export default router
